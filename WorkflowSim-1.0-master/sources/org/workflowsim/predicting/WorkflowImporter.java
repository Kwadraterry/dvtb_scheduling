package org.workflowsim.predicting;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.workflowsim.*;
import org.workflowsim.utils.ClusteringParameters;
import org.workflowsim.utils.OverheadParameters;
import org.workflowsim.utils.Parameters;
import org.workflowsim.utils.ReplicaCatalog;

import java.io.File;
import java.util.Calendar;
import java.util.List;

/**
 * Created by ivan on 11.06.16.
 */
public class WorkflowImporter {
    public static void main(String[] args) {
        try {
            /**
             * Should change this based on real physical path
             */
            String daxPath = "/home/ivan/Repositories/WorkflowSim-1.0-master/Workflow Samples/CyberShake_100.xml";

            File daxFile = new File(daxPath);
            if (!daxFile.exists()) {
                Log.printLine("Warning: Please replace daxPath with the physical path in your working environment!");
                return;
            }

            ReplicaCatalog.init(ReplicaCatalog.FileSystem.LOCAL);

            WorkflowParser parser = new WorkflowParser(daxPath);
            parser.parse();
            WorkflowDbHelper dbHelper = new WorkflowDbHelper("jdbc:postgresql://localhost:5432/workflow", "ivan", "qwe123");
            for (Task task : parser.getTaskList()) {
                TaskExecution execution = new TaskExecution(task.getCloudletLength());
                for (FileItem file : task.getFileList())
                    if(file.getType()== Parameters.FileType.INPUT)
                        execution.getInputSizes().add(file.getSize());
                    else execution.getOutputSizes().add(file.getSize());
                dbHelper.insertTaskExecution(task.getType(), execution);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("The import has been terminated due to an unexpected error");
        }
    }
}
