package org.workflowsim.predicting;

import java.util.*;

public class TaskExecution {
    private long execLength;
    private List<Double> inputSizes;
    private List<Double> outputSizes;

    public TaskExecution(long execLength) {
        this.execLength = execLength;
        this.inputSizes = new ArrayList<>();
        this.outputSizes = new ArrayList<>();
    }

    public void setInputSizes(List<Double> inputSizes) {
        this.inputSizes = inputSizes;
    }

    public void setOutputSizes(List<Double> outputSizes) {
        this.outputSizes = outputSizes;
    }

    public long getExecLength() {
        return this.execLength;
    }

    public List<Double> getInputSizes() {
        return this.inputSizes;
    }

    public List<Double> getOutputSizes() {
        return this.outputSizes;
    }
}

