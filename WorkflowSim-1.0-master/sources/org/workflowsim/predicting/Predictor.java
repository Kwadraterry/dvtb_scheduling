package org.workflowsim.predicting;

import javafx.util.Pair;

import javax.swing.text.MutableAttributeSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Predictor {
    private static int K = 10;

    public static long predictTaskLength(List<Double> curInputSizes, List<TaskExecution> prevExecutions) {
        if(prevExecutions.size()==0) return 100;
        List<Pair<Double, Long>> dists = new ArrayList<>(prevExecutions.size());
        for(TaskExecution exec : prevExecutions)
            dists.add(new Pair<>(getDist(curInputSizes, exec.getInputSizes()), exec.getExecLength()));
        dists.sort((o1, o2) -> {
            if(o1.getKey()<o2.getKey()) return -1;
            if(o1.getKey()>o2.getKey()) return 1;
            return 0;
        });
        double sumRuntime = 0;
        for(int i=0; i < Math.min(K, dists.size()); ++i)
            sumRuntime += dists.get(i).getValue();
        return (long)(sumRuntime / K);
    }

    public static List<Double> predictOutsizes(List<Double> curInputSizes, List<TaskExecution> prevExecutions) {
        if(prevExecutions.size()==0) new ArrayList<Double>();
        List<Pair<Double, Long>> dists = new ArrayList<>(prevExecutions.size());
        for(TaskExecution exec : prevExecutions)
            dists.add(new Pair<>(getDist(curInputSizes, exec.getInputSizes()), exec.getExecLength()));
        dists.sort((o1, o2) -> {
            if(o1.getKey()<o2.getKey()) return -1;
            if(o1.getKey()>o2.getKey()) return 1;
            return 0;
        });
        int maxOutCnt = 0;
        for(TaskExecution exec : prevExecutions)
            maxOutCnt = Math.max(maxOutCnt, exec.getOutputSizes().size());
        List<Double> predictedOutsizes = new ArrayList<>(maxOutCnt);
        for(int i=0; i<maxOutCnt; ++i) {
            predictedOutsizes.add(0.0);
            int cnt = 0;
            for(TaskExecution exec : prevExecutions)
                if(i < exec.getOutputSizes().size()) {
                    cnt++;
                    predictedOutsizes.set(i, predictedOutsizes.get(i) + exec.getOutputSizes().get(i));
                }
            predictedOutsizes.set(i, predictedOutsizes.get(i)/cnt);
        }
        return predictedOutsizes;
    }

    private static double getDist(List<Double> inputs1, List<Double> inputs2) {
        double dist = 0;
        for(int i=0; i< Math.max(inputs1.size(), inputs2.size()); ++i) {
            double size1 = i<inputs1.size()?inputs1.get(i):0;
            double size2 = i<inputs2.size()?inputs2.get(i):0;
            double delta = size1-size2;
            dist += delta*delta;
        }
        return Math.sqrt(dist);
    }
}
