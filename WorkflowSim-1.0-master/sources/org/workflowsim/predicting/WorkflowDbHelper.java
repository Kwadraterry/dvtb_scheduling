package org.workflowsim.predicting;

import javafx.util.Pair;
import org.workflowsim.FileItem;

import java.lang.reflect.Executable;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class WorkflowDbHelper {
    private Connection conn;

    public static Connection getConnection(String url, String username, String password) {
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            conn = null;
        }
        return conn;
    }


    public WorkflowDbHelper(String url, String username, String password) {
        conn = getConnection(url, username, password);
    }

    public int getTaskId(String taskName) {
        if(conn==null) return 0;
        try {
            PreparedStatement pstmt = conn.prepareStatement("SELECT id FROM tasks WHERE name=?");
            pstmt.setString(1, taskName);
            ResultSet result = pstmt.executeQuery();
            int taskId = 0;
            if (result.next())
                taskId = result.getInt("id");
            result.close();
            pstmt.close();
            return taskId;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return 0;
        }
    }

    public List<TaskExecution> getExecutionsByTaskName(String taskName) throws Exception {
        int taskId = getTaskId(taskName);

        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM executions WHERE task_id=?");
        pstmt.setInt(1, taskId);
        ResultSet result = pstmt.executeQuery();
        List<TaskExecution> executions = new ArrayList<>();
        while (result.next()) {
            int execId = result.getInt("id");
            long execLength = result.getLong("tasklength");

            TaskExecution exec = new TaskExecution(execLength);
            exec.setInputSizes(getInputSizes(execId));
            exec.setOutputSizes(getOutputSizes(execId));
            executions.add(exec);
        }
        result.close();
        pstmt.close();
        conn.close();
        return executions;
    }

    public List<Double> getInputSizes(int execId) throws Exception {
        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM input_sizes WHERE exec_id=?");
        pstmt.setInt(1, execId);
        ResultSet result = pstmt.executeQuery();
        List<Double> inputSizes = new ArrayList<>();
        while(result.next()) {
            double fileSize = result.getDouble("file_size");
            inputSizes.add(fileSize);
        }
        return inputSizes;
    }

    public List<Double> getOutputSizes(int execId) throws Exception {
        PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM output_sizes WHERE exec_id=?");
        pstmt.setInt(1, execId);
        ResultSet result = pstmt.executeQuery();
        List<Double> outputSizes = new ArrayList<>();
        while(result.next()) {
            double fileSize = result.getDouble("file_size");
            outputSizes.add(fileSize);
        }
        return outputSizes;
    }

    public void insertTaskExecution(String taskName, TaskExecution execution) throws Exception {
        int taskId = getTaskId(taskName);
        PreparedStatement pstmt = conn.prepareStatement("INSERT INTO executions(task_id, tasklength) VALUES (?, ?)",
                Statement.RETURN_GENERATED_KEYS);
        pstmt.setInt(1, taskId);
        pstmt.setLong(2, execution.getExecLength());
        pstmt.execute();
        int execId = -1;
        ResultSet generatedKeys = pstmt.getGeneratedKeys();
        if(generatedKeys.next())
            execId = generatedKeys.getInt("id");
        pstmt.close();
        for (double inSize : execution.getInputSizes()) {
            PreparedStatement fstmt = conn.prepareStatement("INSERT INTO input_sizes(exec_id, file_size) VALUES (?, ?)");
            fstmt.setInt(1, execId);
            fstmt.setDouble(2, inSize);
            fstmt.execute();
            fstmt.close();
        }
        for (double outSize : execution.getOutputSizes()) {
            PreparedStatement fstmt = conn.prepareStatement("INSERT INTO output_sizes(exec_id, file_size) VALUES (?, ?)");
            fstmt.setInt(1, execId);
            fstmt.setDouble(2, outSize);
            fstmt.execute();
            fstmt.close();
        }
    }
}

