CREATE TABLE tasks
(
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  CONSTRAINT tasks_pkey PRIMARY KEY (id)
);

CREATE TABLE executions
(
  id serial NOT NULL,
  task_id serial NOT NULL,
  taskLength BIGINT NOT NULL,
  CONSTRAINT exec_pkey PRIMARY KEY(id),
  CONSTRAINT taskid_fkey FOREIGN KEY(task_id) REFERENCES tasks(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE input_sizes
(
  id SERIAL NOT NULL,
  exec_id serial NOT NULL,
  file_size REAL NOT NULL,
  CONSTRAINT in_pkey PRIMARY KEY (id),
  CONSTRAINT exec_fkey FOREIGN KEY (exec_id) REFERENCES executions(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE output_sizes
(
  id SERIAL NOT NULL,
  exec_id serial NOT NULL,
  file_size REAL NOT NULL,
  CONSTRAINT out_pkey PRIMARY KEY (id),
  CONSTRAINT exec_fkey FOREIGN KEY (exec_id) REFERENCES executions(id) ON DELETE CASCADE ON UPDATE CASCADE
);
